<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<!doctype html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Code Helper</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/bootstrap/css/bootstrap.min.css"/>"/>
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/style/main.css"/>"/>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/resources/bootstrap/css/bootstrap-responsive.min.css"/>"/>


    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="#">Diary App</a>
            <div class="nav-collapse collapse">
                <ul class="nav">

                    <tiles:useAttribute name="activeTab" ignore="true"/>

                    <li <c:if test="${activeTab == 'home'}">class="active"</c:if>><a href="#">Home</a></li>
                    <li <c:if test="${activeTab == 'entry'}">class="active"</c:if>><a href="#">Entries</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <tiles:insertAttribute name="body"/>
</div>
</body>

<script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>

</html>