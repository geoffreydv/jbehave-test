<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<tiles:insertDefinition name="base-layout">
    <tiles:putAttribute name="body">
        <h1>Home Page</h1>
        <p>Welcome to the home page</p>
    </tiles:putAttribute>
</tiles:insertDefinition>