<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="base-layout">
    <tiles:putAttribute name="body">
        <form:form action="">
            <fieldset>
                <legend>Edit Entry</legend>
                <form:label path="title">Title</form:label>
                <form:input path="title"/>

                <form:label path="content">Content</form:label>
                <form:textarea path="content" rows="5" cols="150" />
            </fieldset>

            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Save Changes</button>
                <button type="submit" class="btn">Cancel</button>
            </div>

        </form:form>
    </tiles:putAttribute>
</tiles:insertDefinition>