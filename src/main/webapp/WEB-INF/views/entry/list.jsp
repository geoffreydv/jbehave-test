<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<tiles:insertDefinition name="base-layout">
    <tiles:putAttribute name="activeTab" value="entry" />
    <tiles:putAttribute name="body">
        <table class="table table-condensed table-hover">
            <tr>
                <th>Date</th>
                <th>Title</th>
                <th>Actions</th>
            </tr>
            <c:forEach items="${entries}" var="entry">
                <tr>
                    <td><fmt:formatDate value="${entry.creationDate.time}" pattern="dd/MM/YYYY" /></td>
                    <td>${entry.title}</td>
                    <td><a class="btn btn-small" href="<c:url value="/entry/1/edit"/>">Edit</a></td>
                </tr>
            </c:forEach>
        </table>
    </tiles:putAttribute>
</tiles:insertDefinition>