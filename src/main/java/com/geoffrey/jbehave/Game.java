package com.geoffrey.jbehave;

public class Game {
    private StringRenderer observer;

    public Game(int width, int height) {

    }

    public void setObserver(StringRenderer observer) {
        this.observer = observer;
    }

    public StringRenderer getObserver() {
        return observer;
    }

    public void toggleCellAt(int column, int row) {

    }
}
