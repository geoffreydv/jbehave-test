package com.geoffrey.web;

import com.geoffrey.diaryapp.model.DiaryEntry;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Controller
@RequestMapping("/entry")
public class DiaryEntryController {

    @RequestMapping("/")
    public String list(Model model) {

        List<DiaryEntry> entries = new ArrayList<DiaryEntry>();

        DiaryEntry entry = new DiaryEntry();

        entry.setCreationDate(Calendar.getInstance());
        entry.setTitle("This is the title");
        entry.setContent("I am the content");

        entries.add(entry);
        entries.add(entry);
        entries.add(entry);

        model.addAttribute("entries", entries);

        return "entry/list";
    }

    @RequestMapping("/{id}/edit")
    public String edit(@PathVariable("id") String entryId, Model model) {

        DiaryEntry entry = new DiaryEntry();

        entry.setCreationDate(Calendar.getInstance());
        entry.setTitle("This is the title");
        entry.setContent("I am the content");

        model.addAttribute("command", entry);

        return "entry/form";
    }

}
